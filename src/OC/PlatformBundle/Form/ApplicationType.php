<?php

namespace OC\PlatformBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ApplicationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cvs','entity', [
                'class'=>'OCUserBundle:Cv',
                'property'=>'title',
                'expanded' => false,
                'multiple' => false,
                'required'=>true,
//                'label' => 'Selectionner un CV',
                'required'=>false,
                'mapped'=>false,
                'attr' =>[
                    'class' => 'form-control'
                ]
            ])
            ->add('document','entity', [
                'class'=>'OCUserBundle:Document',
                'property'=>'name',
                'label' => 'Telecharger un CV',
                'required'=>false,
                'mapped'=>false,
                'attr' =>[
                    'class' => 'form-control'
                ]
            ])
            ->add('content', null, [
                'label' => 'Message privé',
                'required'=>false,
                'attr' =>[
                    'class' => 'form-control'
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OC\PlatformBundle\Entity\Application'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'oc_platformbundle_application';
    }
}
