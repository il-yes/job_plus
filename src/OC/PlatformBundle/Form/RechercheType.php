<?php

namespace OC\PlatformBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RechercheType extends AbstractType
{
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('recherche','text', array('attr' => array('placeholder' => 'ex : intégrateur front...' )))
            ->add('research','text', array('attr' => array('placeholder' => 'lieu...' ), 'required'=>false))
         ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'oc_platformbundle_recherche';
    }
}