<?php

namespace OC\PlatformBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RedirectionListener
{
    public function __construct(ContainerInterface $container, Session $session)
    {
        $this->session = $session;
        $this->router = $container->get('router');
        $this->securityContext = $container->get('security.context');
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $route = $event->getRequest()->attributes->get('_route');

        if ($route == 'oc_platform_application_add') {

            # Condition de securite : verifie si l'internaute est connecté sous son login
            if (!is_object($this->securityContext->getToken()->getUser())) {
                $this->session->getFlashBag()->add('notification','Merci de vous identifier');
//                $this->session->set("redirection_referer",$this->router->generate('front_panier_list'));
                $event->setResponse(new RedirectResponse($this->router->generate('fos_user_security_login')));
            }
        }
        
    }
}