<?php

namespace OC\PlatformBundle\Controller;

use OC\PlatformBundle\Entity\Application;
use OC\PlatformBundle\Form\ApplicationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OC\PlatformBundle\Entity\Advert;
use Symfony\Component\HttpFoundation\Request;

class ApplicationController extends Controller
{
    public function addAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);
        $user = $this->getUser();

        $application = new Application();
        $application->setAuthor($user);

        $form = $this->get('form.factory')->create(new ApplicationType(), $application);

        if ($form->handleRequest($request)->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($application);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Candidature bien enregistrée.');

            return $this->redirect($this->generateUrl('oc_platform_advert_view', array('id' => $advert->getId())));
        }

//        die(var_dump($user));

        return $this->render('OCPlatformBundle:Application:new.html.twig', array(
            'application' => $application,
            'form' => $form->createView()
        ));
    }


    Public function formAction(Request $request, $id = 0)
    {
        $em = $this->getDoctrine()->getManager();
        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);
        $user = $this->getUser()->getId();

        $application = new Application();
        $application->setAuthor($user);
//        die(var_dump($application->getAuthor()));
        // Edit action
        if($id > 0){
            $application = $this->getDoctrine()->getManager()->getRepository("OCPlatformBundle:Application")->find($id);

        }

        // Création form
        $formBuilder = $this->get('form.factory')->createBuilder(new ApplicationType(), $application);


        $form = $formBuilder->getForm();
        $form->add('submit', 'submit', array('attr' => array('class' => 'btn btn-success'), 'label' => 'Ajouter'));



        if($form->handleRequest($request)->isValid())
        {
            $em = $this->getDoctrine()->getManager();
//            die(var_dump($form->getData()));
            $em->persist($application);

            $em->flush();
            $session = $request -> getSession();
            $session -> getFlashBag() -> add("info", "Opération validée : le message a bien été créé !");


            return  $this->redirect($this->generateUrl('oc_platform_advert_view', array('id' => $advert->getId())));
        }



        return $this->render('OCPlatformBundle:Application:new.html.twig', array(
            'application' => $application,
            'advert' => $advert,
            'form' => $form->createView(),
        ));

    }
}