<?php

// src/OC/PlatformBundle/Controller/AdvertController.php

namespace OC\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OC\PlatformBundle\Entity\Advert;
use OC\UserBundle\Entity\User;
use OC\UserBundle\Entity\Address;
use OC\PlatformBundle\Form\AdvertType;
use OC\PlatformBundle\Form\AdvertEditType;
use OC\PlatformBundle\Form\RechercheType;
use OC\UserBundle\Form\AddressType;
use OC\PlatformBundle\Entity\Image;
use OC\PlatformBundle\Entity\Application;
use OC\PlatformBundle\Entity\AdvertSkill;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use OC\PlatformBundle\Bigbrother\BigbrotherEvents;
use OC\PlatformBundle\Bigbrother\MessagePostEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Util\Debug;

class AdvertController extends Controller
{
    public function indexAction($page)
    {
        // On ne sait pas combien de pages il y a
        // Mais on sait qu'une page doit être supérieure ou égale à 1
        if ($page < 1) {
            // On déclenche une exception NotFoundHttpException, cela va afficher
            // une page d'erreur 404 (qu'on pourra personnaliser plus tard d'ailleurs)
            throw new NotFoundHttpException('Page "'.$page.'" inexistante.');
        }
        // On fixe le nb d'annonce a 3
        $nbPerPage = 3;
        // Ici, on récupérera la liste des annonces, puis on la passera au template
        $em = $this -> getDoctrine()->getManager();
        $listAdverts = $em -> getRepository('OCPlatformBundle:Advert')->getAdverts($page, $nbPerPage);
        
        // On calcule le nombre total de pages grâce au count($listAdverts) qui retourne le nombre total d'annonces
        $nbPages = ceil(count($listAdverts)/$nbPerPage);

        // Si la page n'existe pas, on retourne une 404
        if ($page > $nbPages) {
          throw $this->createNotFoundException("La page ".$page." n'existe pas.");
        }

        // Mais pour l'instant, on ne fait qu'appeler le template
        return $this->render('OCPlatformBundle:Advert:index.html.twig', array('listAdverts' => $listAdverts,
                                                                              'page' => $page,
                                                                              'nbPages' => $nbPages,
                                                                             ));
    }


    public function viewAction(Advert $advert)
    {
        // On récupère l'annonce $id
        $em = $this->getDoctrine()->getManager();
        
        
        // On récupère la liste des candidatures de cette annonce
        $listApplications = $em
          ->getRepository('OCPlatformBundle:Application')
          ->findBy(array('advert' => $advert));
        
        // On récupère maintenant la liste des AdvertSkill
        $listAdvertSkills = $em
          ->getRepository('OCPlatformBundle:AdvertSkill')
          ->findBy(array('advert' => $advert));

        return $this->render('OCPlatformBundle:Advert:view.html.twig', array('advert' => $advert,
                                                                             'listApplications' => $listApplications,
                                                                             'listAdvertSkills' => $listAdvertSkills,
                                                                            ));
    }

    public function viewSlugAction(Advert $advert)
    {
        return $this->viewAction($advert);
    }

    
    public function addAction(Request $request)
    {
        
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_AUTEUR')) {
          // Sinon on déclenche une exception « Accès interdit »
          throw new AccessDeniedException('Accès limité aux auteurs.');
        }

        $advert = new Advert();
        $form = $this->get('form.factory')->create(new AdvertType(), $advert);

        if ($form->handleRequest($request)->isValid()) {

          $user = $this->container->get('security.context')->getToken()->getUser();
           // On crée l'évènement avec ses 2 arguments
//          $event = new MessagePostEvent($advert->getContent(), $user);

          // On déclenche l'évènement
//          $this
//            ->get('event_dispatcher')
//            ->dispatch(BigbrotherEvents::onMessagePost, $event)
//          ;

          // On récupère ce qui a été modifié par le ou les listeners, ici le message
//          $advert->setContent($event->getMessage());

          $em = $this->getDoctrine()->getManager();    
          $em->persist($advert);
          $em->flush();

          $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

          return $this->redirect($this->generateUrl('oc_platform_advert_view', array('id' => $advert->getId())));
        }

        return $this->render('OCPlatformBundle:Advert:add.html.twig', array('form' => $form->createView()));

    }


    
    public function editAction($id, Request $request)
    {
        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();

        // On récupère l'entité correspondant à l'id $id
        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);

        // Si l'annonce n'existe pas, on affiche une erreur 404
        if ($advert == null) {
          throw $this->createNotFoundException("L'annonce d'id ".$id." n'existe pas.");
        }

        $form = $this->get('form.factory')->create(new AdvertEditType(), $advert);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // Inutile de persister ici, Doctrine connait déjà notre annonce
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien modifiée.');
            
            return $this->redirect($this->generateUrl('oc_platform_advert_view', array('id' => $advert->getId())));
        }
                
        return $this->render('OCPlatformBundle:Advert:edit.html.twig', array('advert' => $advert,
                                                                             'form' => $form->createView()
                                                                            ));
    }

    
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // On récupère l'annonce $id
        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);

        if (null === $advert) {
          throw new NotFoundHttpException("L'annonce d'id ".$id." n'existe pas.");
        }

        // On crée un formulaire vide, qui ne contiendra que le champ CSRF
        // Cela permet de protéger la suppression d'annonce contre cette faille
        $form = $this->createFormBuilder()->getForm();

        if ($form->handleRequest($request)->isValid()) {
          $em->remove($advert);
          $em->flush();

          $request->getSession()->getFlashBag()->add('info', "L'annonce a bien été supprimée.");

          return $this->redirect($this->generateUrl('oc_platform_advert_index'));
        }

        // Si la requête est en GET, on affiche une page de confirmation avant de delete
        return $this->render('OCPlatformBundle:Advert:delete.html.twig', array('advert' => $advert,
                                                                               'form'   => $form->createView()
                                                                              ));
    }
    
    
    public function menuAction($limit)
    {
        $listAdverts = $this->getDoctrine()
        ->getManager()
        ->getRepository('OCPlatformBundle:Advert')
        ->findBy(
            array(),                 // Pas de critère
            array('date' => 'desc'), // On trie par date décroissante
            $limit,                  // On sélectionne $limit annonces
            0                        // À partir du premier
        );

        return $this->render('OCPlatformBundle:Advert:menu.html.twig', array(
          'listAdverts' => $listAdverts
        ));
    }
    

  public function translationAction($name)
  {
    return $this->render('OCPlatformBundle:Advert:translation.html.twig', array(
      'name' => $name
    ));
  } 

  public function accueilAction()
  {
    return $this->render('OCPlatformBundle:Advert:accueil.html.twig');
  } 


    # Fonction de recherche 
    public function rechercheAction()
    {
        $city = "Paris";
        $defaults = array('research' => $city);
        $form = $this->createForm(new RechercheType(), $defaults);
        //$form = $this -> createForm(new RechercheType());
        $form->add('submit', 'submit', array('label' => 'Chercher', 'attr' => array('class' => 'btn btn-hero btn-md')));
        
        return $this->render('OCPlatformBundle:Search:recherche.html.twig',array("form"=>$form->createView()));
    
    }
    
    
    # Fonction pour une recherche interne
    public function rechercheTraitementAction(Request $request, $lieu = 0, $page)
    {
        // TRAITEMENT DES PAGES
        if ($page < 1) {
            // On déclenche une exception NotFoundHttpException, cela va afficher
            // une page d'erreur 404 (qu'on pourra personnaliser plus tard d'ailleurs)
            throw new NotFoundHttpException('Page "'.$page.'" inexistante.');
        }
        // On fixe le nb d'annonce a 3
        $nbPerPage = 2;
        // Ici, on récupérera la liste des annonces, puis on la passera au template
        $em = $this -> getDoctrine()->getManager();
        $listAdverts = $em -> getRepository('OCPlatformBundle:Advert')->getAdverts($page, $nbPerPage);
        
        // On calcule le nombre total de pages grâce au count($listAdverts) qui retourne le nombre total d'annonces
        $nbPages = ceil(count($listAdverts)/$nbPerPage);

        // Si la page n'existe pas, on retourne une 404
        if ($page > $nbPages) {
          throw $this->createNotFoundException("La page ".$page." n'existe pas.");
        }

        // TRAITEMENT DE LA RECHERCHE
        $em = $this -> getDoctrine()-> getManager();
        $form = $this->createForm(new RechercheType());

        if ($this->get('request')->getMethod() == 'POST')
        {
            $form->bind($this->get('request'));
            //die(Debug::dump($form));
            $em = $this->getDoctrine()->getManager();
            $listAdverts = $em->getRepository('OCPlatformBundle:Advert')->recherche($form['recherche']->getData(), $form['research']->getData());
            
            //    die(Debug::dump($listAdverts));     
        }
        //die();
        
        return $this->render('OCPlatformBundle:Advert:index.html.twig',array("listAdverts"=>$listAdverts,
                                                                             'nbPages' => $nbPages,
                                                                             'page' => $page
                                                                             ));
    }



    # Fonction de recherche 
    public function rechercheExAction()
    {
        $city = "Paris";
        $defaults = array('research' => $city);
        $form = $this->createForm(new RechercheType(), $defaults);
        //$form = $this -> createForm(new RechercheType());
        //$form->add('submit', 'submit', array('label' => 'Chercher', 'attr' => array('class' => 'btn btn-default')));
        
        return $this->render('OCPlatformBundle:Search:search.html.twig',array("form"=>$form->createView()));
    
    } 



    # Fonction pour une recherche externe
    public function rechercheTraitementExAction(Request $request, $lieu = 0, $page)
    {
        $em = $this -> getDoctrine()-> getManager();
        $form = $this->createForm(new RechercheType());

        if ($this->get('request')->getMethod() == 'POST')
        {
            $form->bind($this->get('request'));
            //die(Debug::dump($form));
            $em = $this->getDoctrine()->getManager();
            $listAdverts = $em->getRepository('OCPlatformBundle:Advert')->recherche($form['recherche']->getData(), $form['research']->getData());
            
            die(Debug::dump($listAdverts));     
        }

        
        return $this->render('OCPlatformBundle:Advert:accueil.html.twig',array("listAdverts"=>$listAdverts));
    }
    

}