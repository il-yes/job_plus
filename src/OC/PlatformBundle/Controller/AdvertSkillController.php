<?php
/**
 * Created by PhpStorm.
 * User: Manu
 * Date: 04/06/2016
 * Time: 18:12
 */

namespace OC\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OC\PlatformBundle\Entity\AdvertSkill;
use OC\PlatformBundle\Entity\Advert;
use Doctrine\Common\Util\Debug;

class AdvertSkillController extends Controller
{
    public function indexAction($id)
    {
        $advertSkills = $this->getDoctrine()->getManager()
                             ->getRepository('OCPlatformBundle:AdvertSkill')->MyFindAll($id);
//        var_dump($advertSkills);

        return $this->render('OCPlatformBundle:AdvertSkill:index.html.twig', array( "advertSkills" => $advertSkills ));
    }
}