<?php

namespace OC\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    public function actualitesAction()
    {

        return $this->render('OCPlatformBundle:Page:actualites.html.twig');
    }
}