<?php

namespace OC\PlatformBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/hello/Fabien');

        $this->assertTrue($crawler->filter('html:contains("Hello Fabien")')->count() > 0);
    }
}


// TRAVAIL SUR LE FORMULAIRE D'INSCRIPTION DE LA 3W ACADEMY AVEC DES INPUTS TYPE RADIO ET GOOGLE MAP
///**
// * Affichage des differentes formations (400h, Mobile, Business School) de la ville de Paris.
// */
//$('.panel-selection-paris').hide();                      // Gestion de l'affichage des formations spécifiques a la ville de Paris
//$("#city-paris").on("change", function() {
//    if ($(this).is(':checked')) {
//        $('.panel-selection-paris').show();
//    }
//
//});
//
//
///**
// * Affichage des sessions de formations des villes autres que Paris.
// */
//$("#city .input-city").on("change", function() {      // Affiche la valeur de l'input radio selectionné
//    if ($(this).is(':checked')) {
//        $('.panel-selection-paris').hide();                  // Formations specifiques a Paris cachées
//    }
//});
//
///**
// * Affichage des dates pour la formation 400h & réunion matinale de la ville de Paris.
// */
//$(".input-city-paris").on("change", function() {      // Affiche la valeur de l'input radio selectionné
//    if ($(this).is(':checked')) {
//    }
//    $('#reunion').show();                         // Affichage du select
//
//    $('#reunion').empty();                            // On vide le select
//    showCalendar();
//    $('.calendar .list').empty();                     // On supprime les date-events du calendrier (pour afficher les nouvelles)
//
//    $( ".bd-example-modal-lg" ).trigger( "click" );
//
//    /*if ($('#reunion').val() == 'all') {
//     for(var i = 0; i < sessions.length; i++){
//
//     $('#reunion').append(sessions[i]);
//
//     }
//     } else {*/
//    // On parcoure le tableau d'options, on filtre  et affiche chaque champs selon la valeur du data attribut (Paris & ParisMat)
//    for (var j = 0; j < sessions.length; j++) {
//        if (sessions[j].dataset.city == "Paris") {
//            $('#reunion').append(sessions[j]);
//
//            getDateSession($(sessions[j]).val());           // Traitement des dates
//            contactMe(sessions[j]);
//        }
//        if (sessions[j].dataset.city == "ParisMat") {
//            $('#reunion').append(sessions[j]);
//
//            getDateSession($(sessions[j]).val());           // Traitement des dates
//
//            //calendar.startCalendar().setEvent();
//            contactMe(sessions[j]);
//        }
//    }
//    calendar.startCalendar();                     // On affiche le calendrier
//    // }
//});
//
///**
// * Affichage des dates de sessions en fonction de la ville selectionné (Provinces et Paris->Mobile & Paris->Business School).
// */
//$(".input-city").on("change", function() {      // Affiche la valeur de l'input radio selectionné
//
//
//
//    $('#reunion').empty();                        // On vide le select
//
//    showCalendar();
//
//    $('.calendar .list').empty();
//
//    $( ".bd-example-modal-lg" ).trigger( "click" );
//
//
//    /*if ($('#reunion').val() == 'all') {
//     for(var i = 0; i < sessions.length; i++){
//
//     $('#reunion').append(sessions[i]);
//
//     }
//     } else {*/
//    // On parcoure le tableau d'options, on filtre  et affiche chaque champs selon la valeur du data attribut
//    for (var j = 0; j < sessions.length; j++) {
//        if (sessions[j].dataset.city == val_city) {
//
//            //$('#reunion').append('<option value="all" class="" data-city="all">Toutes les dates</option>');
//            $('#reunion').append(sessions[j]);
//
//            getDateSession($(sessions[j]).val());       // Traitement des dates
//
//            //calendar.startCalendar().setEvent();
//
//            contactMe(sessions[j]);
//
//        }
//
//    }
//    calendar.startCalendar();                           // On affiche le calendrier
//
//    // }
//});
///**
// * Bonus : Même opération dans le sens inverse, on selectionne l'option dans le select
// * et l'input radio est selectionné dans le même temps
// */
//$("#reunion").on("change", function() {                 // recupere la valeur de l'option selectionné
//
//    date_session = $('#reunion').text();
//    //console.log(date_session);
//
//    var city = date_session.split(' ')[1];
//    var radio = $('input[name="list-city"]');
//    for (var i = 0; i < radio.length; i++ ) {
//        if ( radio[i].value == city ) {
//
//            radio[i].checked = true;
//            return;
//        }
//    }
//});

