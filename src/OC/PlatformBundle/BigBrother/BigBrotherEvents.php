<?php
// src/OC/PlatformBundle/Bigbrother/BigbrotherEvents.php

namespace OC\PlatformBundle\BigBrother;

final class BigbrotherEvents 
{
  const onMessagePost = 'oc_platform.bigbrother.post_message';
  // Vos autres évènements…
}