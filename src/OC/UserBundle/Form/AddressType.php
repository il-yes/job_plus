<?php

namespace OC\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street', null, array(
                'label'=>'Rue',
                'attr'=>array(
                    'class'=>'form-control'
                )))
            ->add('city', null, array(
                'label'=>'Ville',
                'attr'=>array(
                    'class'=>'form-control'
                )))
            ->add('cp', null, array(
                'label'=>'Code postal',
                'attr'=>array(
                    'class'=>'form-control'
                )))
            ->add('country', null, array(
                'label'=>'Pays',
                'attr'=>array(
                    'class'=>'form-control'
                )))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OC\UserBundle\Entity\Address'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'oc_userbundle_address';
    }
}
