<?php

namespace OC\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CvType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('poste')
            ->add('location')
            ->add('contrat','choice',array(
//                'label_attr' => array('class' => 'checkbox-inline'),
                'choices' => array(
                    'Stage' => 'Stage',
                    'CDD' => 'CDD',
                    'CDI' => 'CDI',
                    'Free-lance' => 'Free-lance'
                ),
                'expanded'  => true,
            ))
            ->add('degree')
            ->add('postShow')
            ->add('postShow','choice',array(
            'choices' => array(
                'public' => 'Public',
                'private' => 'Privé',
            ),
                'expanded'  => true,
                "data" =>'pu',
            ))
            ->add('career', null, array(
                'attr' => array('style' => 'height: 200px')
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OC\UserBundle\Entity\Cv'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'oc_userbundle_cv';
    }
}
