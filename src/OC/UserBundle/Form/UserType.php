<?php

namespace OC\UserBundle\Form;

use OC\PlatformBundle\Form\ImageType;
use OC\UserBundle\Form\AddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
//use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            -> add('username',null , array(
                'label'=>'Pseudo',
                'required'=>false,
                'attr'=>array(
                    'class'=>'form-control'
                )))
            -> add('email',null , array(
                'label'=>'Mail',
                'required'=>false,
                'attr'=>array(
                    'class'=>'form-control'
                )))
//            ->add('adresses', new AddressType(), array(
//                'data_class'  => null
//            ))
            ->add('adresses', 'collection', array('type' => new AddressType()))
            ->add('image', new ImageType())
            //->add("image", EntityType::class, array(
            //    'entry_type'   => ImageType::class
            //))
//            ->add('image','entity',array('class'=>'OCPlatformBundle:Image','property'=>'url'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OC\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'oc_userbundle_user';
    }
}
