<?php

namespace OC\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="oc_user")
 */
class User extends BaseUser
{


    public function __construct()
    {
        parent::__construct();
        $this->cvs = new ArrayCollection();
        $this->adresses = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }


  /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;


  /**
     * @ORM\OneToMany(targetEntity="OC\UserBundle\Entity\Cv", mappedBy="user")
     */
    private $cvs;


    /**
   * @ORM\OneToOne(targetEntity="OC\PlatformBundle\Entity\Image", cascade={"persist"})
   */
    private $image;


    /**
    * @var Address
     * @ORM\OneToMany(targetEntity="OC\UserBundle\Entity\Address", mappedBy="user", cascade={"persist"})
     */
    private $adresses;


    /**
     * @ORM\OneToMany(targetEntity="OC\PlatformBundle\Entity\Application", mappedBy="user")
     */
    private $applications;


    /**
     * @ORM\OneToMany(targetEntity="OC\UserBundle\Entity\Document", mappedBy="user")
     */
    private $documents;



    /**
     * Add cv
     *
     * @param \OC\UserBundle\Entity\Cv $cv
     *
     * @return User
     */
    public function addCv(\OC\UserBundle\Entity\Cv $cv)
    {
        $this->cvs[] = $cv;

        return $this;
    }

    /**
     * Remove cv
     *
     * @param \OC\UserBundle\Entity\Cv $cv
     */
    public function removeCv(\OC\UserBundle\Entity\Cv $cv)
    {
        $this->cvs->removeElement($cv);
    }

    /**
     * Get cvs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCvs()
    {
        return $this->cvs;
    }

    /**
     * Set image
     *
     * @param \OC\PlatformBundle\Entity\Image $image
     *
     * @return User
     */
    public function setImage(\OC\PlatformBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \OC\PlatformBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add adress
     *
     * @param \OC\UserBundle\Entity\Address $adress
     *
     * @return User
     */
    public function addAdress(\OC\UserBundle\Entity\Address $adress)
    {
        $this->adresses[] = $adress;

        return $this;
    }

    /**
     * Remove adress
     *
     * @param \OC\UserBundle\Entity\Address $adress
     */
    public function removeAdress(\OC\UserBundle\Entity\Address $adress)
    {
        $this->adresses->removeElement($adress);
    }

    /**
     * Get adresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdresses()
    {
        return $this->adresses;
    }

    /**
     * Add application
     *
     * @param \OC\PlatformBundle\Entity\Application $application
     *
     * @return User
     */
    public function addApplication(\OC\PlatformBundle\Entity\Application $application)
    {
        $this->applications[] = $application;

        return $this;
    }

    /**
     * Remove application
     *
     * @param \OC\PlatformBundle\Entity\Application $application
     */
    public function removeApplication(\OC\PlatformBundle\Entity\Application $application)
    {
        $this->applications->removeElement($application);
    }

    /**
     * Get applications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * Add document
     *
     * @param \OC\UserBundle\Entity\Document $document
     *
     * @return User
     */
    public function addDocument(\OC\UserBundle\Entity\Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \OC\UserBundle\Entity\Document $document
     */
    public function removeDocument(\OC\UserBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
