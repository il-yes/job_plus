<?php

// src/OC/UserBundle/Controller/CvController.php

namespace OC\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OC\UserBundle\Entity\Cv;
use OC\UserBundle\Entity\User;
use OC\UserBundle\Form\CvType;
use Symfony\Component\HttpFoundation\Request;
use OC\PlatformBundle\Bigbrother\BigbrotherEvents;
use OC\PlatformBundle\Bigbrother\MessagePostEvent;


class CvController extends Controller
{


  public function indexAction($id)
  {
      $em = $this->getDoctrine()->getManager();

      $my_all_cv = $em->getRepository('OCUserBundle:Cv')->myFindAllBy($id);

      return $this->render('OCUserBundle:Cv:index.html.twig', array(
          'my_all_cv' => $my_all_cv,
      ));
  }



  public function showAction($number)
  {

    //$id = $this->container->get('security.context')->getToken()->getUser();
    $myCv = $this->getDoctrine()->getManager()->getRepository('OCUserBundle:Cv')->myFindBy($number);

    //die(var_dump($myCv));
    return $this->render('OCUserBundle:Cv:show.html.twig', array('myCv'=>$myCv));
  }


  public function nbCvByUserAction($id)
  {

    $number = $this->getDoctrine()->getManager()->getRepository('OCUserBundle:Cv')->getNbCvByUser($id);

    //die(var_dump($number));
    return $this->render('OCUserBundle:Cv:number_cv.html.twig', array('number'=>$number));
  }


	public function addAction(Request $request)
  {
      $cv = new Cv();
//      die(var_dump($this->getUser()));
      $cv->setUser($this->getUser());
      $form = $this->get('form.factory')->create(new CvType(), $cv);
      $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array('class' => 'btn btn-hero btn-md')));

      if ($form->handleRequest($request)->isValid()) {


        $em = $this->getDoctrine()->getManager();   
        $cv->setUser($this->container->get('security.context')->getToken()->getUser()); 
        $em->persist($cv);
        $em->flush();

        $request->getSession()->getFlashBag()->add('info', 'Votre cv bien été enregistré.');

        return $this->redirect($this->generateUrl('oc_user_cv_add'));
      }

      return $this->render('OCUserBundle:Cv:add.html.twig', array('form' => $form->createView()));

  }


    public function editAction($id, Request $request)
    {
        $em = $this -> getDoctrine()->getManager();
        $cv = $em -> getRepository('OCUserBundle:Cv')->find($id);

        $form = $this->createForm(new CvType(), $cv);
//        die(var_dump($this->getUser()->getId()));
        $form->add('submit', 'submit', array('label' => 'Editer', 'attr' => array('class' => 'btn btn-default marg-top')));
    
        if("POST" === $request -> getMethod())
        {
            $form->handleRequest($request); #permet de recuperer les infos du formulaire
            // Contraintes de validations :
            if($form -> isValid())
            {
                $em -> flush(); #Sauvegarde de l'objet
                $session = $request -> getSession();
                $session -> getFlashBag() -> add("info", "Opération validée : mise à jour effectuée !");
//                die(var_dump($cv));
                return $this->redirect($this -> generateUrl('oc_user_user_show', array('id'=>$this->getUser()->getId())));
            }
        }

        return $this->render('OCUserBundle:Cv:edit.html.twig', array('form' => $form->createView()));

    }



    public function putPrivateAction($id, Request $request)
    {
        $em = $this -> getDoctrine()->getManager();
        $cv = $em -> getRepository('OCUserBundle:Cv')->find($id);
        $cv->setPostShow("Private");

        $em -> flush(); #Sauvegarde de l'objet
        $session = $request -> getSession();
        $session -> getFlashBag() -> add("info", "Opération validée : Votre CV est privé !");


        return $this->redirect($this -> generateUrl('oc_user_user_show', array('id'=>$this->getUser()->getId())));
    }

    public function putPublicAction($id, Request $request)
    {
        $em = $this -> getDoctrine()->getManager();
        $cv = $em -> getRepository('OCUserBundle:Cv')->find($id);
        $cv->setPostShow("Public");

        $em -> flush(); #Sauvegarde de l'objet
        $session = $request -> getSession();
        $session -> getFlashBag() -> add("info", "Opération validée : Votre CV est public !");


        return $this->redirect($this -> generateUrl('oc_user_user_show', array('id'=>$this->getUser()->getId())));
    }


}












