-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 19 Janvier 2016 à 08:10
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `jobplus`
--

-- --------------------------------------------------------

--
-- Structure de la table `advert_category`
--

CREATE TABLE IF NOT EXISTS `advert_category` (
  `advert_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`advert_id`,`category_id`),
  KEY `IDX_84EEA340D07ECCB6` (`advert_id`),
  KEY `IDX_84EEA34012469DE2` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `advert_category`
--

INSERT INTO `advert_category` (`advert_id`, `category_id`) VALUES
(48, 11),
(50, 11),
(51, 14),
(52, 12),
(53, 13);

-- --------------------------------------------------------

--
-- Structure de la table `oc_address`
--

CREATE TABLE IF NOT EXISTS `oc_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `oc_address`
--

INSERT INTO `oc_address` (`id`, `street`, `city`, `code_postal`, `country`) VALUES
(1, '15 av Parmentier', 'Paris', '75011', 'France'),
(2, '15 rue richer', 'Paris', '75019', 'France'),
(3, '38 bis av République', 'Boulogne-billancourt', '92300', 'France'),
(4, '37 bld Richard Lenoir', 'Paris', '75011', 'France'),
(5, '48 bld tour Maubourg', 'Lyon', '69300', 'France');

-- --------------------------------------------------------

--
-- Structure de la table `oc_advert`
--

CREATE TABLE IF NOT EXISTS `oc_advert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `nb_applications` int(11) NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B193175989D9B62` (`slug`),
  UNIQUE KEY `UNIQ_B1931753DA5256D` (`image_id`),
  UNIQUE KEY `UNIQ_B193175F5B7AF75` (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=54 ;

--
-- Contenu de la table `oc_advert`
--

INSERT INTO `oc_advert` (`id`, `date`, `title`, `author`, `content`, `published`, `image_id`, `updated_at`, `nb_applications`, `slug`, `address_id`) VALUES
(48, '0000-00-00 00:00:00', 'Recherche developpeur', 'Jean', 'Recherche developpeur', 1, 41, '2015-08-10 16:38:13', 1, 'developpeur', 2),
(50, '0000-00-00 00:00:00', 'Recherche developpeur/integrateur', 'Jean', 'Recherche developpeur', 1, 42, '2015-08-10 16:39:04', 1, 'developpement', 3),
(51, '2015-08-11 00:00:00', 'Recherche integrateur', 'Ronin', 'Mission integration sur Symfony2 : \r\nTwig, Bootstrap, JQuery', 1, 43, NULL, 0, 'recherche-integrateur', 4),
(52, '2015-08-11 12:20:49', 'Recherche integrateur', 'kate', 'Mission integration web responsive', 1, 44, '2016-01-10 17:32:45', 0, 'recherche-integrateur-1', 1),
(53, '2016-01-18 00:00:00', 'web designer', 'Jean', 'Recherche urgent dev front', 1, 45, NULL, 0, 'web-designer', 5);

-- --------------------------------------------------------

--
-- Structure de la table `oc_advertskill`
--

CREATE TABLE IF NOT EXISTS `oc_advertskill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `advert_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D79B5538D07ECCB6` (`advert_id`),
  KEY `IDX_D79B55385585C142` (`skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `oc_application`
--

CREATE TABLE IF NOT EXISTS `oc_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `advert_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_39F85DD8D07ECCB6` (`advert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `oc_category`
--

CREATE TABLE IF NOT EXISTS `oc_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Contenu de la table `oc_category`
--

INSERT INTO `oc_category` (`id`, `name`) VALUES
(11, 'Développement web'),
(12, 'Développement mobile'),
(13, 'Graphisme'),
(14, 'Intégration'),
(15, 'Réseau');

-- --------------------------------------------------------

--
-- Structure de la table `oc_cv`
--

CREATE TABLE IF NOT EXISTS `oc_cv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `poste` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_emploi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contrat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diplome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `afficher` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parcours` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_6485CA8CA76ED395` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `oc_cv`
--

INSERT INTO `oc_cv` (`id`, `user_id`, `poste`, `titre`, `region_emploi`, `contrat`, `diplome`, `afficher`, `parcours`) VALUES
(1, 2, 'developpeur', 'test user', 'ile de france', 'cdd', 'bts web', 'public', NULL),
(2, 3, 'developpeur', 'dev web php', 'Ile de france', 'cdi', 'formation web', 'public', 'Quisque eget facilisis justo. Phasellus vitae ligula lectus. Morbi venenatis elit id blandit iaculis. Nullam eget dictum tortor. Vestibulum ligula mi, dapibus dapibus viverra a, varius id elit. Aliquam suscipit, magna at convallis euismod, odio purus hendrerit nunc, in volutpat ligula magna a mi. Ut volutpat id nulla eu iaculis. Morbi venenatis eros id sapien facilisis, id interdum diam pellentesque. Fusce ut enim sit amet quam lobortis porta at ac metus.');

-- --------------------------------------------------------

--
-- Structure de la table `oc_image`
--

CREATE TABLE IF NOT EXISTS `oc_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Contenu de la table `oc_image`
--

INSERT INTO `oc_image` (`id`, `url`, `alt`) VALUES
(41, 'jpeg', 'le-seigneur-des-anneaux-4138906qcfew.jpg'),
(42, 'jpeg', 'stock-vector-golden-crown-vector-130860776.jpg'),
(43, 'jpeg', 'photographe.jpg'),
(44, 'jpeg', 'lac.jpg'),
(45, 'jpeg', '16.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `oc_skill`
--

CREATE TABLE IF NOT EXISTS `oc_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Contenu de la table `oc_skill`
--

INSERT INTO `oc_skill` (`id`, `name`) VALUES
(8, 'PHP'),
(9, 'Symfony2'),
(10, 'C++'),
(11, 'Java'),
(12, 'Photoshop'),
(13, 'Blender');

-- --------------------------------------------------------

--
-- Structure de la table `oc_user`
--

CREATE TABLE IF NOT EXISTS `oc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7866CFC992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_7866CFC9A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `oc_user`
--

INSERT INTO `oc_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
(1, 'manuel', 'manuel', 'manuel@mail.com', 'manuel@mail.com', 1, '9bktrzt81ickgcc4ckggwkgw0cw4w8g', 'YjahNyVFKQb2pnXx+mI+R3m+tLDdygaDvgneHFgfZBqqcrKgOjRbt3qIAaeyahoWjBoQg6kU0QChYdXk0157nw==', '2015-08-23 14:13:23', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(2, 'kate', 'kate', 'kate@mail.com', 'kate@mail.com', 1, 'ca0z88kc5fs4ok0w084w8gg0c8wc8ws', 'T5l8TvJbAnoYh9L1QwBToLjslvJZFs3khTdj17RzSVp6oXIet1GXiOxy7+L/Eor1HZy0k/qsd5YA1ZkyHh0NaA==', '2015-11-26 16:52:32', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(3, 'admin', 'admin', 'admin@mail.com', 'admin@mail.com', 1, 'fy7zxy5dgf4k40gg8kc8c4ksww8gw4c', 't7x5JQlUdbZ0vr2RsJNYMBRM4k38rj0sVYsh5MNf5j5we6/GdsYEqcoTN7Yshyo1r/0q8AALIVWBh5g+o8VSig==', '2016-01-18 22:36:53', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `research`
--

CREATE TABLE IF NOT EXISTS `research` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poste` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lieu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `advert_category`
--
ALTER TABLE `advert_category`
  ADD CONSTRAINT `FK_84EEA34012469DE2` FOREIGN KEY (`category_id`) REFERENCES `oc_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_84EEA340D07ECCB6` FOREIGN KEY (`advert_id`) REFERENCES `oc_advert` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `oc_advert`
--
ALTER TABLE `oc_advert`
  ADD CONSTRAINT `FK_B1931753DA5256D` FOREIGN KEY (`image_id`) REFERENCES `oc_image` (`id`),
  ADD CONSTRAINT `FK_B193175F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `oc_address` (`id`);

--
-- Contraintes pour la table `oc_advertskill`
--
ALTER TABLE `oc_advertskill`
  ADD CONSTRAINT `FK_165A7A3C5585C142` FOREIGN KEY (`skill_id`) REFERENCES `oc_skill` (`id`),
  ADD CONSTRAINT `FK_165A7A3CD07ECCB6` FOREIGN KEY (`advert_id`) REFERENCES `oc_advert` (`id`);

--
-- Contraintes pour la table `oc_application`
--
ALTER TABLE `oc_application`
  ADD CONSTRAINT `FK_39F85DD8D07ECCB6` FOREIGN KEY (`advert_id`) REFERENCES `oc_advert` (`id`);

--
-- Contraintes pour la table `oc_cv`
--
ALTER TABLE `oc_cv`
  ADD CONSTRAINT `FK_6485CA8CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `oc_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
